LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY tb_decoder IS
END tb_decoder;
 
ARCHITECTURE behavior OF tb_decoder IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT decoder
    PORT(
			CLK : IN std_logic;
			RST : IN std_logic;
			codigo : IN std_logic_vector(2 DOWNTO 0);
			accion : IN  STD_LOGIC_VECTOR (1 DOWNTO 0);
			led : OUT std_logic_vector(6 DOWNTO 0);
			led_ctrl : OUT  std_logic_vector(3 DOWNTO 0)
			);
    END COMPONENT;
        

   --Inputs
	signal CLK_s : std_logic := '0';
   signal RST_s : std_logic := '0';
   signal codigo_s : std_logic_vector(2 downto 0) := (others => '0');
	signal accion_s: std_logic_vector(1 downto 0) := (others => '0');

 	--Outputs
   signal led_s : std_logic_vector(6 downto 0);
   signal led_ctrl_s : std_logic_vector(3 downto 0);
	
	constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: decoder PORT MAP (
			 CLK => CLK_s,
          RST => RST_s,
          codigo => codigo_s,
			 accion => accion_s,
          led => led_s,
          led_ctrl => led_ctrl_s
        );
 
 -- Clock process definitions
   CLK_process :process
   begin
		CLK_s <= '0';
		wait for CLK_period/2;
		CLK_s <= '1';
		wait for CLK_period/2;
   end process;
	
	
   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		RST_s <= '0';
		codigo_s <= "000"; --NO PULSAR
		accion_s <="01"; --PARADO
		WAIT FOR 20 ns;
		codigo_s <= "011"; -- PULSO 3
		accion_s <="11"; -- SUBIENDO
		WAIT FOR 20 ns;
		codigo_s <= "000"; -- NO PULSO
		accion_s <= "01"; -- PARADO
		WAIT FOR 20 ns;
        codigo_s <= "010"; --PULSO 1
        accion_s <="00"; --BAJANDO
		WAIT FOR 20 ns;
		RST_s <= '1'; --RESET ACTIVO HAGA LO QUE HAGA LED = 1111110
		codigo_s <= "010"; --PULSO 2
		accion_s <="11";--SUBIENDO
		WAIT FOR 20 ns;
		codigo_s <= "100";--PULSO 4
		accion_s <="11"; --SUBIENDO
		WAIT FOR 20 ns;
		RST_s <= '0'; --RESET DESACTIVADO
		codigo_s <= "001";--PULSO 1
		accion_s <="00"; --BAJANDO
		WAIT FOR 20 ns;
		
		ASSERT false
			REPORT "Simulación finalizada. Test superado."
			SEVERITY FAILURE;
   end process;
END;