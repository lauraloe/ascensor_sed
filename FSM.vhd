library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	USE ieee.std_logic_arith.ALL;
	USE ieee.std_logic_unsigned.ALL;
	

	entity FSM is   -- Maquina de estados
		PORT(
		 clock: IN STD_LOGIC;
		 reset: IN STD_LOGIC;
		 nivel: IN STD_LOGIC; --Indica si el ascensor se encuentra entre dos niveles
		 abierto: IN STD_LOGIC;
		 cerrado: IN STD_LOGIC;
		 piso,boton: IN STD_LOGIC_VECTOR (2 DOWNTO 0); --boton "000" es que no se ha pulsado ningun boton
		 destino: out STD_LOGIC_VECTOR (2 DOWNTO 0);
		accionador_puerta: out STD_LOGIC;
		 accion_motor: out std_logic_vector (1 DOWNTO 0) --10 subir, 01 bajar 
		 );
	end FSM;

Architecture Behavioral of FSM is
	

		TYPE estado IS (inicio,parado,cerrando,marcha,abriendo);
	   SIGNAL presente: estado:=inicio;
	   SIGNAL boton_almacenado: STD_LOGIC_VECTOR(2 DOWNTO 0); -- Almacena bot�n pulsado   
	   SIGNAL piso_inicial: STD_LOGIC_VECTOR(2 DOWNTO 0);   

		
	begin
	

	estados:
	PROCESS(reset,clock)
		BEGIN
			IF reset='1' THEN presente<=inicio;
			ELSIF clock='1' AND clock'event THEN
				CASE presente IS
					WHEN inicio=>   -- Estado inicial para que se nivele
						IF nivel='1' then presente<=parado;
						END IF;
					WHEN parado=>    -- Espera la pulsaci�n de un bot�n
						IF (boton_almacenado/="000") AND (boton_almacenado/=piso) THEN presente<=cerrando;
						END IF;
					WHEN cerrando=>  -- Cierra la puerta
						IF cerrado='1' THEN presente<=marcha;
						END IF;
					WHEN marcha=>    -- Lleva el ascensor a su piso
						IF (boton_almacenado=piso) AND (nivel='1') THEN presente<=abriendo;
						END IF;                                              
					WHEN abriendo=>  -- Abre las puertas
						IF abierto='1' THEN presente<=parado;
						END IF;
				END CASE;
			END IF;
		END PROCESS estados;
		
		
	salida:
	PROCESS(clock)
	BEGIN	
		if rising_edge(clock) then
			destino<=boton_almacenado;
			CASE presente IS
			WHEN inicio=>   -- Al encender puede que este entre dos pisos
				IF piso/="001" THEN   --Piso 1 es el punto de partida
						accion_motor <= "01"; -- Bajamos al piso inicial
				END IF;
				Accionador_puerta<='0';     -- Cerrada
			  
			WHEN parado=>
				accion_motor <= "00"; -- El ascensor esta parado
				accionador_puerta<='1'; -- Abierta
			  
			WHEN cerrando=>
				accion_motor <= "00";
				accionador_puerta<='0';   -- Cerrado           
			  		  
			WHEN marcha=>
				IF boton_almacenado<piso_inicial THEN
						accion_motor <= "01"; -- Bajamos hasta el piso deseado
				ELSE
						accion_motor <= "10"; --Subimos
				END IF;
				accionador_puerta<='0';     -- Cerrada
			  
			WHEN abriendo=>
				accion_motor <= "00"; --Parado
				accionador_puerta<='1'; -- Abrir
			END CASE;
		end if;
	END PROCESS salida; 
	
	memoria:
	PROCESS(reset,clock,piso)   -- Captura la pulsaci�n del bot�n y el piso donde nos encontramos
    BEGIN                
       IF reset='1' THEN
         Boton_almacenado<="000";
         piso_inicial<=piso;
       ELSIF clock='1' AND clock'event THEN
         IF presente=parado THEN
           IF (boton="001") OR (boton="010") OR (boton="011") or (boton="100") THEN 
           boton_almacenado<=boton;
           ELSE boton_almacenado<="000";  -- Cualquier otra combinaci�n no vale
           END IF;
           piso_inicial<=piso;
         END IF;
       END IF;
    END PROCESS memoria;


end architecture;


