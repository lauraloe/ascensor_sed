LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	 
	ENTITY tb_motor_ascensor IS
	END tb_motor_ascensor;
	 
	ARCHITECTURE behavior OF tb_motor_ascensor IS 
	 
	    -- Component Declaration for the Unit Under Test (UUT)
	 
	    COMPONENT Motor_ascensor
	    PORT(
	         clk : IN  std_logic;
	         reset : IN  std_logic;
	         accion_motor : in std_logic_vector (1 downto 0);
	         motor_subir : OUT  std_logic;
	         motor_bajar : OUT  std_logic
	        );
	    END COMPONENT;
	    
	   --Inputs
	   signal clk_s : std_logic := '0';
	   signal reset_s : std_logic := '0';
	   signal accion_motor_s : std_logic_vector (1 downto 0);
	   
	
	 	--Outputs
	   signal motor_subir_s : std_logic;
	   signal motor_bajar_s : std_logic;
	

	   -- Clock period definitions
	   constant CLK_period : time := 10 ns;
	 
	BEGIN
	 
		-- Instantiate the Unit Under Test (UUT)
	   uut: Motor_ascensor PORT MAP (
	          clk => clk_s,
	          reset => reset_s,
	          accion_motor => accion_motor_s,
	          motor_subir => motor_subir_s,
	          motor_bajar => motor_bajar_s
	        );
	

	   -- Clock process definitions
	   CLK_process :process
	   begin
			clk_s <= '0';
			wait for CLK_period/2;
			clk_s <= '1';
			wait for CLK_period/2;
	   end process;
	 
	

	   -- Stimulus process
	   stim_proc: process
	   begin		
			
			reset_s <= '0';
			WAIT FOR 5 ns;
			
	       accion_motor_s <= "10"; --subiendo
			WAIT FOR 10 ns;
			
			accion_motor_s <= "11";--parado
			WAIT FOR 10 ns;
			
			accion_motor_s <= "01";--bajando
			WAIT FOR 15 ns;
			
			accion_motor_s <= "00";--parado
			WAIT FOR 10 ns;
			
			reset_s <= '1'; --motor_subir y motor_bajar a cero
			WAIT FOR 5 ns;
	
			accion_motor_s <= "00";--bajando
			--motor_subir y motor_bajar a cero porque reset_s es 1
			WAIT FOR 5 ns;
			
			ASSERT false
				REPORT "Simulación finalizada. Test superado."
				SEVERITY FAILURE;
				
	   end process;
	

	END;

