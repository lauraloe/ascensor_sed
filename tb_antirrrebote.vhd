LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	 
	ENTITY tb_antirrebote IS
	END tb_antirrebote;
	 
	ARCHITECTURE behavior OF tb_antirrebote IS 
	 
	    -- Component Declaration for the Unit Under Test (UUT)
	 
	    COMPONENT antirrebote
	    PORT(
	         CLK : IN  std_logic;
	         RST : IN  std_logic;
	         Entrada_logica : IN  std_logic;
	         Salida_logica : OUT  std_logic
	        );
	    END COMPONENT;
	    
	

	   --Inputs
	   signal CLK_s : std_logic := '0';
	   signal RST_s : std_logic := '0';
	   signal Entrada_logica_s : std_logic := '0';
	

	 	--Outputs
	   signal Salida_logica_s : std_logic;
	

	   -- Clock period definitions
	   constant CLK_period : time := 10 ns;
	 
	BEGIN
	 
		-- Instantiate the Unit Under Test (UUT)
	   uut: antirrebote PORT MAP (
	          CLK => CLK_s,
	          RST => RST_s,
	          Entrada_logica => Entrada_logica_s,
	          Salida_logica => Salida_logica_s
	        );
	

	   -- Clock process definitions
	   CLK_process :process
	   Begin
			CLK_s <= '0';
			wait for CLK_period/2;
			CLK_s <= '1';
			wait for CLK_period/2;
	   end process;
	 
	

	   -- Stimulus process
	   stim_proc: process
	   begin		
		RST_s <= '0';
		Entrada_logica_s <= '1';
		WAIT FOR 20 ns;
		Entrada_logica_s <= '0';
		WAIT FOR 10 ns;
		RST_s <= '1';
		WAIT FOR 20 ns;
		Entrada_logica_s <= '1';
		WAIT FOR 10 ns;
		Entrada_logica_s <= '0';
		WAIT FOR 10 ns;
		RST_s <= '0';
		WAIT FOR 20 ns;
		Entrada_logica_s <= '0';
		WAIT FOR 10 ns;
		Entrada_logica_s <= '1';
		WAIT FOR 20 ns;
		Entrada_logica_s <= '0';
		WAIT FOR 10 ns;
		Entrada_logica_s <= '1';
		WAIT FOR 30 ns;
		
		ASSERT false
				REPORT "Simulación finalizada. Test superado."
				SEVERITY FAILURE;
				
	   end process;
	

	END;


