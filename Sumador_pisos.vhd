library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.ALL;
use ieee.std_logic_unsigned.ALL;


entity Sumador_pisos is
 port(
 clk: in std_logic;
 reset : in std_logic;
 accion_motor : in std_logic_vector (1 downto 0 );
 piso_sumado : out std_logic_vector (2 downto 0)
 );
 
end Sumador_pisos;

architecture Behavioral of Sumador_pisos is

signal piso_actual : std_logic_vector (2 downto 0):="001";

begin
    process (clk, reset)
	begin
		if reset = '1' then
			piso_actual <= "001";
		elsif rising_edge (clk) then
			if accion_motor = "10" and piso_actual /= "100" then
				piso_actual <= piso_actual + 1;
			elsif accion_motor = "01" and piso_actual /= "001" then
				piso_actual <= piso_actual -1 ;
			end if;
		end if;
		
		piso_sumado<=piso_actual;
	end process;


end Behavioral;
