library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity antirrebote_vector is
    Port ( CLK : in  STD_LOGIC;
			  RST : in  STD_LOGIC;
           vector_IN : in  STD_LOGIC_VECTOR (3 downto 0);
           vector_OUT : out  STD_LOGIC_VECTOR(3 downto 0));
end antirrebote_vector;

architecture Dataflow of antirrebote_vector is

COMPONENT antirrebote
	PORT (
		CLK : in  STD_LOGIC;
		RST : in  STD_LOGIC;
      Entrada_logica : in  STD_LOGIC;
      Salida_logica : out  STD_LOGIC);
END COMPONENT;

begin

inst_antirrebote_1:
antirrebote port map(
		CLK => CLK,
		RST => RST,
		Entrada_logica => vector_IN(0),
		Salida_logica => vector_OUT(0)
		);

inst_antirrebote_2:
antirrebote port map(
		CLK => CLK,
		RST => RST,
		Entrada_logica => vector_IN(1),
		Salida_logica => vector_OUT(1)
		);
		
inst_antirrebote_3:
antirrebote port map(
		CLK => CLK,
		RST => RST,
		Entrada_logica => vector_IN(2),
		Salida_logica => vector_OUT(2)
		);
		
inst_antirrebote_4:
        antirrebote port map(
                CLK => CLK,
                RST => RST,
                Entrada_logica => vector_IN(3),
                Salida_logica => vector_OUT(3)
                );
                
end Dataflow;

