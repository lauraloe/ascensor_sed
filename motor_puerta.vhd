library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity motor_puerta is
    Port ( 
              CLK : in  STD_LOGIC;
			  RST : in  STD_LOGIC;
			  nivel : in STD_LOGIC;
			  Sensor_detec: in  STD_LOGIC;
              accionar_puerta : in  STD_LOGIC;
              dir_puerta : out  STD_LOGIC);
end motor_puerta;

architecture Behavioral of motor_puerta is

begin
Control_puerta 
	:process(RST,CLK)
	begin
		if (RST='1') then
			dir_puerta <= '1'; --la puerta se abre
		elsif rising_edge(CLK) then
			if (accionar_puerta='1' AND nivel='0') then  -- la puerta se quiere abrir, y el ascensor esta entre 2 pisos
				dir_puerta<= '0'; --la puerta no se abre
				
			elsif (accionar_puerta='1' AND nivel='1') then -- la puerta se quiere abrir, y el ascensor esta en un piso
			 dir_puerta<= '1'; --la puerta se abre
			 
			elsif (accionar_puerta='0' AND Sensor_detec='1' AND nivel='1') then  --la puerta se quiere cerrar, hay algo entre medias, y estamos parados en un piso
				dir_puerta <= '1'; --la puerta se abre
				
			--elsif (accionar_puerta='0' AND sensor_detec = '0' AND nivel='1') then
			--     dir_puerta<= '0'; --la puerta se cierra
			     
			else dir_puerta <= '0'; -- si no hay problema la puerta se cierra
			end if;
		end if;
	end process;
end Behavioral;


