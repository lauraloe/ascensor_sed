LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	

	ENTITY tb_Convertidor_Piso IS
	END tb_Convertidor_Piso;
	 
	ARCHITECTURE behavior OF tb_Convertidor_Piso IS 
	 
	    -- Component Declaration for the Unit Under Test (UUT)
	 
	    COMPONENT Convertidor_Piso
	    PORT(
	         clk : IN  std_logic;
	         rst : IN  std_logic;
	         piso : IN  std_logic_vector(3 downto 0);
	         boton_pulsado : IN  std_logic_vector(3 downto 0);
	         piso_convertido : OUT  std_logic_vector(2 downto 0);
	         boton_pulsado_convertido : OUT  std_logic_vector(2 downto 0)
	        );
	    END COMPONENT;
	    
	

	   --Inputs
	   signal clk_s : std_logic := '0';
	   signal rst_s : std_logic := '0';
	   signal piso_s : std_logic_vector(3 downto 0) := (others => '0');
	   signal boton_pulsado_s : std_logic_vector(3 downto 0) := (others => '0');
	

	 	--Outputs
	   signal piso_convertido_s : std_logic_vector(2 downto 0);
	   signal boton_pulsado_convertido_s : std_logic_vector(2 downto 0);
	

	   -- Clock period definitions
	   constant clk_period : time := 10 ns;
	 
	BEGIN
	 
		-- Instantiate the Unit Under Test (UUT)
	   uut: Convertidor_Piso PORT MAP (
	          clk => clk_s,
	          rst => rst_s,
	          piso => piso_s,
	          boton_pulsado => boton_pulsado_s,
	          piso_convertido => piso_convertido_s,
	          boton_pulsado_convertido => boton_pulsado_convertido_s
	        );
	

	   -- Clock process definitions
	   clk_process :process
	   begin
			clk_s <= '0';
			wait for clk_period/2;
			clk_s <= '1';
			wait for clk_period/2;
	   end process;
	 
	

	   -- Stimulus process
	   stim_proc: process
	   begin		
		
		rst_s <= '0';
		boton_pulsado_s <= "0001"; 
		piso_s <= "0001"; 
		WAIT FOR 20 ns;
		boton_pulsado_s <= "1000";
		piso_s <= "0100";
		WAIT FOR 10 ns;
		boton_pulsado_s <= "0010";
		piso_s  <= "0100";
		WAIT FOR 10 ns;
		rst_s <= '1';
        WAIT FOR 20 ns;
        rst_s <= '0';
        WAIT FOR 30 ns;
		boton_pulsado_s <= "0101";
		piso_s <= "0010";
		WAIT FOR 20 ns;
		boton_pulsado_s <= "0100";
		piso_s  <= "0101";
		WAIT FOR 10 ns;
		boton_pulsado_s <= "0010";
		piso_s  <= "0010";
		WAIT FOR 10 ns;
		boton_pulsado_s <= "1000";
		piso_s  <= "0100";
		WAIT FOR 10 ns;
		boton_pulsado_s <= "0010";
		piso_s  <= "1000";
		WAIT FOR 20 ns;
		boton_pulsado_s <= "0001";
		piso_s <= "0010";
		WAIT FOR 30 ns;
		
		ASSERT false
				REPORT "Simulación finalizada. Test superado."
				SEVERITY FAILURE;
				
	   end process;
	

	END;

