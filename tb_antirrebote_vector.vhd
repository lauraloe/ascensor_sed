LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	

	ENTITY tb_antirrebote_vector IS
	END tb_antirrebote_vector;
	 
	ARCHITECTURE behavior OF tb_antirrebote_vector IS 
	 
	    -- Component Declaration for the Unit Under Test (UUT)
	 
	    COMPONENT antirrebote_vector
	    PORT(
	         CLK : IN  std_logic;
	         RST : IN  std_logic;
	         vector_IN : IN  std_logic_vector(3 downto 0);
	         vector_OUT : OUT  std_logic_vector(3 downto 0)
	        );
	    END COMPONENT;
	    
	

	   --Inputs
	   signal CLK_s : std_logic := '0';
	   signal RST_s : std_logic := '0';
	   signal Entrada_logica_s : std_logic_vector(3 downto 0) := (others => '0');
	

	 	--Outputs
	   signal Salida_logica_s : std_logic_vector(3 downto 0);
	

	   -- Clock period definitions
	   constant CLK_period : time := 10 ns;
	 
	BEGIN
	 
		-- Instantiate the Unit Under Test (UUT)
	   uut: antirrebote_vector PORT MAP (
	          CLK => CLK_s,
	          RST => RST_s,
	          vector_IN => Entrada_logica_s,
	          vector_OUT => Salida_logica_s
	        );
	

	   -- Clock process definitions
	   CLK_process :process
	   Begin
			CLK_s <= '0';
			wait for CLK_period/2;
			CLK_s <= '1';
			wait for CLK_period/2;
	   end process;
	 
	

	   -- Stimulus process
	   stim_proc: process
	   begin		
		
		WAIT FOR 5 ns;
		RST_s <= '0';
		Entrada_logica_s <= "0000";
		WAIT FOR 20 ns;
		RST_s <= '1';
		WAIT FOR 10 ns;
		Entrada_logica_s <= "1000";
		WAIT FOR 15 ns;
		RST_s <= '0';
		WAIT FOR 20 ns;
		Entrada_logica_s <= "0010";
		WAIT FOR 10 ns;
		Entrada_logica_s <= "1000";
		WAIT FOR 20 ns;
		Entrada_logica_s <= "0100";
		WAIT FOR 10 ns;
		Entrada_logica_s <= "0100";
		WAIT FOR 10 ns;
		Entrada_logica_s <= "1001";
		WAIT FOR 5 ns;
		Entrada_logica_s <= "0010";
		WAIT FOR 20 ns;
		Entrada_logica_s <= "1000";
		WAIT FOR 30 ns;
		ASSERT false
				REPORT "Simulación finalizada. Test superado."
				SEVERITY FAILURE;
				
	   end process;
	

	END;

