LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	 
	ENTITY tb_gestor_display IS
	END tb_gestor_display;
	 
	ARCHITECTURE behavior OF tb_gestor_display IS 
	 
	    -- Component Declaration for the Unit Under Test (UUT)
	 
	    COMPONENT gestor_display
	    PORT(
	         CLK : IN  std_logic;
	         piso_inicial : IN  std_logic_vector(2 downto 0);
	         piso_destino : IN  std_logic_vector(2 downto 0);
	         piso_seleccionado : OUT  std_logic_vector(2 downto 0);
	         piso_actual : OUT  std_logic_vector(2 downto 0);
	         accion : OUT  std_logic_vector(1 downto 0)
	        );
	    END COMPONENT;
	    
	
	   --Inputs
	   signal CLK_s : std_logic := '0';
	   signal piso_inicial_s : std_logic_vector(2 downto 0) := (others => '0');
	   signal piso_destino_s : std_logic_vector(2 downto 0) := (others => '0');
	

	 	--Outputs
	   signal piso_seleccionado_s : std_logic_vector(2 downto 0);
	   signal piso_actual_s : std_logic_vector(2 downto 0);
	   signal accion_s : std_logic_vector(1 downto 0);
	

	   -- Clock period definitions
	   constant CLK_period : time := 10 ns;
	 
	BEGIN
	 
		-- Instantiate the Unit Under Test (UUT)
	   uut: gestor_display PORT MAP (
	          CLK => CLK_s,
	          piso_inicial => piso_inicial_s,
	          piso_destino => piso_destino_s,
	          piso_seleccionado => piso_seleccionado_s,
	          piso_actual => piso_actual_s,
	          accion => accion_s
	        );
	

	   -- Clock process definitions
	   CLK_process :process
	   begin
			CLK_s <= '0';
			wait for CLK_period/2;
			CLK_s <= '1';
			wait for CLK_period/2;
	   end process;
	 
	   -- Stimulus process
	   stim_proc: process
	   begin		
	      WAIT FOR 10 ns;
			piso_inicial_s <= "001";--1
			piso_destino_s <= "011";--3
			--subiendo
			WAIT FOR 20 ns;
			piso_inicial_s <= "011";--3
			--piso_obj <= "011"; --3
			--parado = 2
			WAIT FOR 20 ns;
			piso_destino_s <= "001";--1
			--bajando
			WAIT FOR 20 ns;
			piso_destino_s <= "100";
			--subiendo
			WAIT FOR 20 ns;
			
			ASSERT false
				REPORT "Simulación finalizada. Test superado."
				SEVERITY FAILURE;
	   end process;
	

	END;

