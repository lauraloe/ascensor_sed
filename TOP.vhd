library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TOP is
PORT(
--  Entradas al top
	  boton_seleccionado: IN std_logic_vector(3 DOWNTO 0); 
      piso: IN std_logic_vector(3 DOWNTO 0);
      nivel: IN std_logic;
      sensor_detec: IN std_logic;
      abierto: IN std_logic;
      cerrado:  IN std_logic;
	
-- Salidas al top	  
	  puerta: OUT std_logic;
	  accion_motor: OUT std_logic_vector(1 DOWNTO 0);	   
	  piso1_sel, piso2_sel, piso3_sel,piso4_sel: OUT std_logic;
	  led: OUT std_logic_vector(7 DOWNTO 0);
	  led_ctrl: OUT std_logic_vector(3 DOWNTO 0);
	  
-- clk antes del divisor de frecuencias
	  clk: in std_logic;										
	  reset: in std_logic 
	  );
	  
end TOP;

architecture Behavioral of TOP is

COMPONENT convertidor_piso
	PORT(
		clk : in STD_LOGIC;
		rst : in  STD_LOGIC;
		piso: IN std_logic_vector(3 DOWNTO 0);
		boton_pulsado: IN std_logic_vector(3 DOWNTO 0);
		piso_convertido: OUT std_logic_vector( 2 DOWNTO 0);
		boton_pulsado_convertido: OUT std_logic_vector(2 DOWNTO 0)
		);
 END COMPONENT;
 
COMPONENT Divisor_frecuencia
	PORT (
		clk : in STD_LOGIC;
		reset : in STD_LOGIC;
		clk_out:out STD_LOGIC
	);
 END COMPONENT;
 
COMPONENT FSM
	PORT(
	 clock: IN std_logic;
	 reset: IN std_logic;
	 nivel: IN std_logic;
	 abierto: IN std_logic;
	 cerrado:  IN std_logic;
	 piso: IN STD_LOGIC_VECTOR (2 DOWNTO 0);
	 boton :IN STD_LOGIC_VECTOR (2 DOWNTO 0);
	 destino: out STD_LOGIC_VECTOR (2 DOWNTO 0);
	 accionador_puerta: out STD_LOGIC;
	 accion_motor: out STD_LOGIC_VECTOR(1 DOWNTO 0)
	 );
END COMPONENT;

COMPONENT gestor_display
	PORT (
		CLK : in  STD_LOGIC;
        piso_inicial : in  STD_LOGIC_VECTOR (2 downto 0);
        piso_destino : in  STD_LOGIC_VECTOR (2 downto 0);
		piso_seleccionado : out STD_LOGIC_VECTOR (2 downto 0);
		piso_actual : out  STD_LOGIC_VECTOR (2 downto 0);
		accion : out  STD_LOGIC_VECTOR (1 DOWNTO 0)
	);
END COMPONENT;

COMPONENT decoder
	PORT (
		CLK : IN std_logic;
		RST : IN std_logic;
		codigo : IN std_logic_vector(2 DOWNTO 0);
		accion : IN  STD_LOGIC_VECTOR (1 DOWNTO 0);
		led : OUT std_logic_vector(6 DOWNTO 0);
		led_ctrl : OUT  std_logic_vector(3 DOWNTO 0)
	);
 END COMPONENT;

COMPONENT Decoder_piso
	PORT (
	  codigo_piso : in  STD_LOGIC_VECTOR (3 downto 0);
      piso1 : out  STD_LOGIC;
      piso2 : out  STD_LOGIC;
      piso3 : out  STD_LOGIC;
      piso4 : out  STD_LOGIC
	);
END COMPONENT;

COMPONENT motor_puerta
	PORT (
		CLK : in  STD_LOGIC;
		RST : in  STD_LOGIC;
		nivel : in STD_LOGIC;
  		sensor_detec : in  STD_LOGIC;
        accionar_puerta : in  STD_LOGIC;
        dir_puerta : out  STD_LOGIC
	);
END COMPONENT;

COMPONENT motor_ascensor 
	PORT(
		CLK : in  STD_LOGIC;
		RST : in  STD_LOGIC;
		accion_motor: in STD_LOGIC_VECTOR (1 DOWNTO 0);
		motor_subir: out  STD_LOGIC;
		motor_bajar : out  STD_LOGIC
	);
END COMPONENT;

 signal senal_puerta:std_logic;  --Senal para motor_puerta
 signal senal_motor: std_logic_vector(1 DOWNTO 0);
 signal senal_subir: std_logic;
 signal senal_bajar: std_logic;
 signal senal_ffdiv:std_logic;  --Senal del divisor de frecuencia
 signal senal_piso_actual:std_logic_vector (2 DOWNTO 0);
 signal senal_piso_destino:std_logic_vector (2 DOWNTO 0);
 signal senal_boton_pulsado:std_logic_vector (2 DOWNTO 0);
 signal codigo_piso_actual:std_logic_vector (2 DOWNTO 0);
 signal codigo_piso_destino:std_logic_vector (2 DOWNTO 0);
 signal senal_accion:std_logic_vector (1 DOWNTO 0);
 
begin

inst_convertidor_piso:Convertidor_piso port map(
		clk => clk,
		rst => reset,
		piso=> piso,
		boton_pulsado => boton_seleccionado,
		piso_convertido => senal_piso_actual,
		boton_pulsado_convertido => senal_piso_destino
		);		
		
inst_Divisor_frecuencia: Divisor_frecuencia port map(
		clk => clk,
		reset => reset,
		clk_out => senal_ffdiv
		);
		
inst_FSM:FSM port map(
		abierto => abierto,
		cerrado => cerrado,
		clock => senal_ffdiv,
		reset => reset,
		nivel => nivel,
		piso => senal_piso_actual,
		boton => senal_piso_destino,
		destino => senal_boton_pulsado,
		accionador_puerta => senal_puerta,
		accion_motor=> senal_motor
	   
		);
		
inst_gestor_display:Gestor_display port map(
		CLK => senal_ffdiv,
        piso_inicial =>senal_piso_actual,
        piso_destino => senal_boton_pulsado,
		piso_seleccionado => codigo_piso_destino,
		piso_actual => codigo_piso_actual,
		accion => senal_accion
	);
		
inst_decoder: Decoder port map(
		CLK => senal_ffdiv,
		RST => reset,
		codigo => codigo_piso_actual,
		accion => senal_accion,
		led => led (7 downto 1),
		led_ctrl => led_ctrl
	);
	led(0) <= '1';			--punto decimal
		
inst_dec_piso_seleccion: Decoder_piso port map(
		codigo_piso => codigo_piso_destino,
		piso1 => piso1_sel,
		piso2 => piso2_sel,
		piso3 => piso3_sel,
		piso4 => piso4_sel
	);
	
inst_motor_puerta:motor_puerta port map(
		CLK => clk,
		RST => reset,
		nivel => nivel,
	  	sensor_detec => sensor_detec,
      accionar_puerta => senal_puerta,
      dir_puerta => puerta
	);
		
inst_motor_ascensor:motor_ascensor port map(
		CLK => clk,
		RST => reset,
		accion_motor => senal_motor,
		motor_subir => senal_subir,
		motor_bajar => senal_bajar
	);
	
end Behavioral;