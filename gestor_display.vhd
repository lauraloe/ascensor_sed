library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Gestor_Display is
    Port (
           CLK : in  STD_LOGIC;
           piso_inicial: in  STD_LOGIC_VECTOR (2 downto 0);
           piso_destino : in  STD_LOGIC_VECTOR (2 downto 0);
		   piso_seleccionado : out STD_LOGIC_VECTOR (2 downto 0);
           piso_actual : out  STD_LOGIC_VECTOR (2 downto 0);
           accion : out  STD_LOGIC_VECTOR (1 downto 0)
			);
end Gestor_display;

architecture Behavioral of Gestor_display is 

signal piso_act:STD_LOGIC_VECTOR (2 downto 0); --Asignar valor a la salida una vez se haya pulsado un boton

begin

gestor_display:
process(clk)
	begin
		if rising_edge(clk) then
		
			if (piso_inicial/="000") then
				piso_act <= piso_inicial;
			end if;
			
			piso_actual <= piso_act;
			piso_seleccionado <= piso_destino;
			
			
			if (piso_destino= "000") then
				accion <= "01";
			elsif (piso_act < piso_destino) then
				accion <= "11";
			elsif (piso_act > piso_destino) then
				accion <= "00";
			else
				accion <= "10";
			end if;
		end if;
	end process;

end Behavioral;


