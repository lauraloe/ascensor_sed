library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity antirrebote is
    Port (
           CLK : in  STD_LOGIC;					--Entrada de reloj sin pasar por el divisor
		   RST : in  STD_LOGIC;
           Entrada_logica : in  STD_LOGIC;
           Salida_logica : out  STD_LOGIC);
end antirrebote;

architecture Behavioral of antirrebote is

--El tiempo antirrebote depender� del tama�o del contador y de la frecuencia f segun (2^n)/f => NYQUIST

constant contador_tamano : integer := 20;		--tama�o del contador (Para 50MHz: 19, 10ms - 20, 20ms - 21, 42ms - 22, 84ms)

signal boton_alm   : std_logic := '0';		--almacena estado del boton, se usa como variable de transicion
signal contador    : std_logic_vector(contador_tamano downto 0) := (others => '0');	--vector contador para que pase el tiempo antirrebote
	 
begin
    process(CLK,RST,Entrada_logica)
    begin
	 if (RST='1') then
		Salida_logica <= Entrada_logica;
		
	 elsif (CLK'event and CLK='1') then									--Si hay flanco de reloj
		 if (boton_alm XOR Entrada_logica)='1' then							--Si 	la entrada es diferente al estado previo almacenado
			 contador <= (others => '0');										--Contador a cero
		 	 boton_alm <= Entrada_logica;											--La entrada pasa a la variable de transicion
		 elsif (contador(contador_tamano) <= '0') then					--Si la entrada es igual a la variable de transicion y el contador no esta lleno
			 contador <= std_logic_vector(UNSIGNED(contador) + 1);	--Sumamos 1 al contador
       else																		--Si la entrada es igual a la variable de transicion y el contador esta lleno
			 Salida_logica <= boton_alm;										--Pasamos la variable de transicion a la salida
		 end if;
	 end if;
	 
    end process;

end Behavioral;

