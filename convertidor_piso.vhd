
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

entity Convertidor_Piso is
	PORT(
		clk : in STD_LOGIC;
		rst : in  STD_LOGIC;
		piso: IN std_logic_vector(3 DOWNTO 0);
		boton_pulsado: IN std_logic_vector(3 DOWNTO 0);
		--Convierto un vector de 4 bits a uno de 3 bits
		piso_convertido: OUT std_logic_vector(2 DOWNTO 0);
		boton_pulsado_convertido: OUT std_logic_vector(2 DOWNTO 0)
		);
end Convertidor_piso;

architecture dataflow of Convertidor_piso is

COMPONENT vector_antirrebote
	PORT (
		CLK : in  STD_LOGIC;
		RST : in  STD_LOGIC;
        vector_IN : in  STD_LOGIC_VECTOR (3 downto 0);
		vector_OUT : out  STD_LOGIC_VECTOR (3 downto 0));
 END COMPONENT;
 
 signal boton_antirrebote:std_logic_vector(3 DOWNTO 0);

begin

inst_vector_antirrebote:
vector_antirrebote port map(
		CLK => clk,
		RST => rst,
		vector_IN => boton_pulsado,
		vector_OUT => boton_antirrebote
		);
		
	WITH piso SELECT
				piso_convertido <=  "001" WHEN "0001",
									"010" WHEN "0010",
									"011" WHEN "0100",
									"100" WHEN "1000",
									"000" WHEN others;
	WITH boton_antirrebote SELECT
				boton_pulsado_convertido <= "001" WHEN "0001",
											"010" WHEN "0010",
											"011" WHEN "0100",
											"100" WHEN "1000",
											"000" WHEN others;
end dataflow;



