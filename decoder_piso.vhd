library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	USE ieee.std_logic_arith.ALL;
	USE ieee.std_logic_unsigned.ALL;
	
	entity Decoder_piso is
	    Port ( 
	           codigo_piso : in  STD_LOGIC_VECTOR (3 downto 0);
	           piso1 : out  STD_LOGIC;
	           piso2 : out  STD_LOGIC;
                         piso3 : out  STD_LOGIC;
	           piso4 : out  STD_LOGIC);
	end entity Decoder_piso;
	

	architecture dataflow of Decoder_piso is
	

	begin
	

		WITH codigo_piso SELECT
					piso1 <= '1' WHEN "0001",
								'0' WHEN others;
							
		WITH codigo_piso SELECT						
					piso2 <= '1' WHEN "0010",
								'0' WHEN others;
							
		WITH codigo_piso SELECT						
					piso3 <= '1' WHEN "0100",
								'0' WHEN others;
        WITH codigo_piso SELECT
					piso4 <= '1' WHEN "1000",
								'0' WHEN others;

end architecture dataflow;


