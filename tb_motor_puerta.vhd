LIBRARY ieee;
	USE ieee.std_logic_1164.ALL;
	 
	ENTITY tb_motor_puerta IS
	END tb_motor_puerta;
	 
	ARCHITECTURE behavior OF tb_motor_puerta IS 
	 
	    -- Component Declaration for the Unit Under Test (UUT)
	 
	    COMPONENT motor_puerta
	    PORT(
	         CLK : IN  std_logic;
	         RST : IN  std_logic;
	         nivel : IN  std_logic;
	         Sensor_detec : IN  std_logic;
	         accionar_puerta : IN  std_logic;
	         dir_puerta : OUT  std_logic
	        );
	    END COMPONENT;
	    
	

	   --Inputs
	   signal CLK_s : std_logic := '0';
	   signal RST_s : std_logic := '0';
	   signal nivel_s : std_logic := '0';
	   signal Sensor_detec_s : std_logic := '0';
	   signal accionar_puerta_s : std_logic := '0';
	

	 	--Outputs
	   signal dir_puerta_s : std_logic;
	

	   -- Clock period definitions
	   constant CLK_period : time := 10 ns;
	 
	BEGIN
	 
		-- Instantiate the Unit Under Test (UUT)
	   uut: motor_puerta PORT MAP (
	          CLK => CLK_s,
	          RST => RST_s,
	          nivel => nivel_s,
	          Sensor_detec => Sensor_detec_s,
	          accionar_puerta => accionar_puerta_s,
	          dir_puerta => dir_puerta_s
	        );
	

	   -- Clock process definitions
	   CLK_process :process
	   begin
			CLK_s <= '0';
			wait for CLK_period/2;
			CLK_s <= '1';
			wait for CLK_period/2;
	   end process;
	 
	

	   -- Stimulus process
	   stim_proc: process
	   begin		
		
			RST_s <= '0';
	        Sensor_detec_s <= '0';
			WAIT FOR 5 ns;
			accionar_puerta_s <= '1';
			WAIT FOR 10 ns;
			accionar_puerta_s <= '0';
			Sensor_detec_s <= '0';
			WAIT FOR 10 ns;
			nivel_s <= '1';
			WAIT FOR 20 ns;
			RST_s <= '1';
			WAIT FOR 5 ns;
			RST_s <= '0';
			WAIT FOR 10 ns;
			Sensor_detec_s <= '1';
			WAIT FOR 10 ns;
			accionar_puerta_s <= '1';
			WAIT FOR 20 ns;
			Sensor_detec_s <= '0';
			WAIT FOR 10 ns;
			nivel_s <= '0';
			WAIT FOR 10 ns;
			nivel_s <= '1';
			WAIT FOR 15 ns;
			
			ASSERT false
				REPORT "Simulacion finalizada. Test superado."
				SEVERITY FAILURE;
		
	   end process;
	

	END;

